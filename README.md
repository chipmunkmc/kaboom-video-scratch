# kaboom-video-scratch

[This](https://www.youtube.com/watch?v=QF9dpy5qSvE) video about the kaboom.pw anarchy server, but ported to a Scratch project. Available at scratch.mit.edu [here](https://scratch.mit.edu/projects/1045597999/).

To run this, download the repository as a zip, rename it to have the `.sb3` extension, and then load it from the Scratch editor.
